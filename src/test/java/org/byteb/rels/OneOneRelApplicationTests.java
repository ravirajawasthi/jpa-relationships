package org.byteb.rels;

import org.byteb.rels.entities.Email;
import org.byteb.rels.entities.Message;
import org.byteb.rels.repos.EmailRepository;
import org.byteb.rels.repos.MessageRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
class OneOneRelApplicationTests {

    @Autowired
    EmailRepository emailRepository;

    @Autowired
    MessageRepository messageRepository;

    @Test
    void emailAndMessageTest() {
        Message message = new Message(null,null,"Important Message");
        messageRepository.save(message);
        Email email = new Email(null,message,"URGENTT U WIINN 99898989898");
        email = emailRepository.save(email);

        Optional<Email> foundEmail = emailRepository.findById(email.getId());
        assertNotNull(foundEmail.get());
    }

}
