package org.byteb.rels.mto;

import com.github.javafaker.Faker;
import jakarta.persistence.EntityManager;
import org.byteb.rels.repos.BooksRepository;
import org.byteb.rels.repos.PublisherRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

@SpringBootTest
public class BooksPublisherTest {

    private final Faker faker = new Faker();
    @Autowired
    EntityManager entityManager;
    @Autowired
    BooksRepository bookRepository;


    @Autowired
    PublisherRepository publisherRepository;


    @Test
    @Transactional
    void bookPublisherSaveTest() {

        var publisher = new Publisher();
        publisher.setName("Midgard Times");

        var savedPublisher = publisherRepository.save(publisher);

        assertNull(publisher.getBooks());

        Books[] booksToSave = new Books[ 10 ];

        IntStream.range(0,10).forEach(n -> {
            booksToSave[ n ] = new Books(null,faker.book().title(),savedPublisher);
        });
        bookRepository.saveAllAndFlush(List.of(booksToSave));

        entityManager.refresh(savedPublisher);

        assertEquals(10,savedPublisher.getBooks().size());
        savedPublisher.getBooks().stream().limit(10).forEach(books -> assertEquals("Midgard Times",
                                                                                   books.getPublisher().getName()));
    }

}
