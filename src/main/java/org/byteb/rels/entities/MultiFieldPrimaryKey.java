package org.byteb.rels.entities;

import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@Embeddable
public class MultiFieldPrimaryKey implements Serializable {

    @Column(name = "firstname")
    String firstname;

    @Column(name = "lastname")
    String lastname;

}