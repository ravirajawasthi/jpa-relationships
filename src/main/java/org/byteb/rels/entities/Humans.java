package org.byteb.rels.entities;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity(name = "humans")
public class Humans {

    @Id
    MultiFieldPrimaryKey id;

}
