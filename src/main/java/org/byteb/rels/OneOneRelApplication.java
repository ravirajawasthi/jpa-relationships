package org.byteb.rels;

import org.byteb.rels.oto.Home;
import org.byteb.rels.oto.HomeOwner;
import org.byteb.rels.repos.HomeOwnerRepository;
import org.byteb.rels.repos.HomeRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class OneOneRelApplication {


    public static void main(String[] args) {
        SpringApplication.run(OneOneRelApplication.class,args);
    }

    @Bean
    CommandLineRunner initDatabase(HomeRepository homeRepository,HomeOwnerRepository homeOwnerRepository) {

        return args -> {
            Home h1 = new Home();
            h1.setAddress("Midgard");
            homeRepository.save(h1);

            HomeOwner ho1 = new HomeOwner();
            ho1.setName("Atreus");
            ho1.setHome(h1);

            homeOwnerRepository.save(ho1);
        };
    }
}
