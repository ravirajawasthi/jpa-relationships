package org.byteb.rels.controller;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.byteb.rels.entities.Email;
import org.byteb.rels.entities.Humans;
import org.byteb.rels.entities.Message;
import org.byteb.rels.entities.MultiFieldPrimaryKey;
import org.byteb.rels.repos.EmailRepository;
import org.byteb.rels.repos.HumanRepository;
import org.byteb.rels.repos.MessageRepository;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

import static org.byteb.rels.util.Constants.ARTHUR;
import static org.byteb.rels.util.Constants.MORGAN;

@Slf4j
@RestController
@AllArgsConstructor
public class OneToOneController {

    private final EmailRepository emailRepository;
    private final MessageRepository messageRepository;

    private final HumanRepository humanRepository;


    @GetMapping("/email")
    public Email getEmail() {
        Message message = new Message(null,null,"Important Message");
        messageRepository.save(message);
        Email email = new Email(null,message,"URGENTT U WIINN 99898989898");
        log.info(emailRepository.save(email).getId());
        email.setSubject("SUBJECT CHANGED");
        log.info(emailRepository.save(email).getId());
        email.setSubject("SUBJECT AGAIN");
        log.info(emailRepository.save(email).getId());
        return email;
    }

    @GetMapping("/message")
    public Message getMessage() {
        Email email = new Email(null,null,"URGENTT U WIINN 99898989898");
        email = emailRepository.save(email);
        Message message = new Message(null,email,"Important Message");
        message = messageRepository.save(message);


        return message;
    }


    @GetMapping("/humans")
    Humans createNewHuman() throws Exception {
        Humans human = new Humans();

        MultiFieldPrimaryKey id = new MultiFieldPrimaryKey();
        id.setFirstname(ARTHUR);
        id.setLastname(MORGAN);
        human.setId(id);

        humanRepository.save(human);

        MultiFieldPrimaryKey id2 = new MultiFieldPrimaryKey();
        id2.setFirstname(ARTHUR);
        id2.setLastname(null);
        Optional<Humans> foundHuman =
                humanRepository.findById(id2);
        if (foundHuman.isPresent()) {
            throw new Exception("Should not work!!");


        }

        return human;
    }


}
