package org.byteb.rels.repos;

import org.byteb.rels.entities.Humans;
import org.byteb.rels.entities.MultiFieldPrimaryKey;
import org.springframework.data.jpa.repository.JpaRepository;

public interface HumanRepository extends JpaRepository<Humans, MultiFieldPrimaryKey> {
}
