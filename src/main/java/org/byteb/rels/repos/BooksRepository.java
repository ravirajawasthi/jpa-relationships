package org.byteb.rels.repos;

import org.byteb.rels.mto.Books;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BooksRepository extends JpaRepository<Books, String> {
}
