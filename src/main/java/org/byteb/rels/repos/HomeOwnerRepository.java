package org.byteb.rels.repos;

import org.byteb.rels.oto.HomeOwner;
import org.springframework.data.jpa.repository.JpaRepository;

public interface HomeOwnerRepository extends JpaRepository<HomeOwner, String> {
}
