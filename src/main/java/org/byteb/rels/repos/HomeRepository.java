package org.byteb.rels.repos;

import org.byteb.rels.oto.Home;
import org.springframework.data.jpa.repository.JpaRepository;

public interface HomeRepository extends JpaRepository<Home, String> {
}
