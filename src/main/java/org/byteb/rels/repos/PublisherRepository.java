package org.byteb.rels.repos;

import org.byteb.rels.mto.Publisher;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PublisherRepository extends JpaRepository<Publisher, String> {
}
