package org.byteb.rels.oto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
@Table(name = "home")
@Entity(name = "home")
public class Home {

    /* Home will have no idea the HomeOwner exists */

    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    String id;

    @Column
    String address;


    @OneToOne(mappedBy = "home") //where ever mapped by is there. opposite entity has the foreign_key field
    @JsonIgnore //Else will create a infinite loop trying to deserialize everything
    private HomeOwner homeOwner;

}
