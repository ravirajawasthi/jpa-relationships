package org.byteb.rels.oto;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
@Entity(name = "homeOwner")
@Table(name = "home_owner")
public class HomeOwner {

    /* Home Owner will have a primary key field */

    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    String id;

    @Column(name = "name")
    String name;

    @OneToOne
    Home home;

}
